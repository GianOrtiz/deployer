local kube = import 'kube.libsonnet';
local ingress = import 'ingress.libsonnet';

{
  tasksUrlHost(name, ingressUrlSuffix):: name + '.tasks.' + ingressUrlSuffix,
  tasksServiceName(name):: name+'-tasks',
  tasksIngress(name, ingressUrlSuffix, port):: 
    local svcName = self.tasksServiceName(name);
    ingress(svcName, [{
      host: $.tasksUrlHost(name, ingressUrlSuffix),
      serviceName: svcName,
      port: port
    }], httpsRedirect=false),
}

