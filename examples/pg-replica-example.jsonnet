local postgres = import "../postgres.libsonnet";

local postgresUrl(dbName) = 'postgres://postgres:secret@postgres:5432/'+dbName;

function () {
  pg: postgres.ReplicatedPostgres('testdb', 'postgres', 'postgres', 'secret', 'repl_user', 'repl_password', 3),
}