local kube = import "../kube.libsonnet";

local postgresUrl(dbName) = 'postgres://postgres:secret@postgres:5432/'+dbName;

function (image='example', env='test', dbBackupServiceAccountJson=null) {
  example: {
    container:: kube.Container('example', [4004], {
      DATABASE_URL: postgresUrl('example'),
    }) {
      image: image
    },
    svc: kube.Service('example', self.container),
    deploy: kube.Deployment('example', self.container),
  },
  pg: kube.Postgres('postgres', 'postgres', 'secret'),
  bkp: if dbBackupServiceAccountJson != null then
    kube.PostgresBackupCron('example', postgresUrl('example'), '/backup-folder/example', 'gcp-id', dbBackupServiceAccountJson)
}