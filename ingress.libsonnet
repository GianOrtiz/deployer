// This ingress resource generates an URL 
// that points to the provided service
// ---
// it supports HTTPS(TLS);
// it is implied that there is a "clusterissuer"
// resource with a DNS01 solver;
function (name, configs, httpsRedirect=true) {
  apiVersion: "extensions/v1beta1",
  kind: "Ingress",
  metadata: {
    annotations: {
      "kubernetes.io/ingress.class": "nginx",
      "kubernetes.io/tls-acme": "true", // tells ingress-shim to watch this resource and to generate new certificates if this changes
      "nginx.ingress.kubernetes.io/ssl-redirect": if httpsRedirect then "true" else "false", // if http is reached, should it redirect to https?
      "certmanager.k8s.io/acme-challenge-type": "dns01",
      "certmanager.k8s.io/acme-dns01-provider": "cloudDNS",
      "cert-manager.io/cluster-issuer": "letsencrypt-prod",
    },
    name: name,
  },
  spec: {
    rules: [
      {
        host: config.host,
        http: {
          paths: [{ backend: {
            serviceName: config.serviceName,
            servicePort: config.port
          }}]
        }
      } for config in configs ],
    tls: [{
      hosts: [config.host for config in configs],
      secretName: name+'-ingress-certificate-secret'
    }],
  }
}
